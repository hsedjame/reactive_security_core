package io.gitlab.hsedjame.project.security.core.services;

import io.gitlab.hsedjame.project.security.core.models.User;


/**
 * @Project MYALDOC
 * @Author Henri Joel SEDJAME
 * @Date 09/12/2018
 * @Class purposes : .......
 */
public interface NotificationSender {

    void notifyAccountCreation(User user);

    void notifyAccountDeletion(User user);

    void notifyAccountActivation(User user);
}
