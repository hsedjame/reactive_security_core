package io.gitlab.hsedjame.project.security.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectSecurityCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectSecurityCoreApplication.class, args);
    }

}
