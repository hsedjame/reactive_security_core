package io.gitlab.hsedjame.project.security.core.repositories;

import io.gitlab.hsedjame.project.security.core.models.Role;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface RoleRepository extends ReactiveMongoRepository<Role, String> {

  Mono<Role> findByRoleName(String rolename);
}
