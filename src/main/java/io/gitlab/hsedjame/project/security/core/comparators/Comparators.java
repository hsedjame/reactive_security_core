package io.gitlab.hsedjame.project.security.core.comparators;

import io.gitlab.hsedjame.project.security.core.models.Role;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Comparator;

/**
 * @Project MYALDOC
 * @Author Henri Joel SEDJAME
 * @Date 13/11/2018
 * @Class purposes : .......
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Comparators {
    public static final Comparator<Role> ROLE_COMPARATOR = Comparator.comparing(Role::getRoleName);
}
